#!/bin/sh

ROOT=$(pwd)
ZOLA=$1

echo "Removing old builder"
rm ./builder/target/release/builder

echo "Clearing any old public files"
rm -rf ./public

echo "Records directory size before build:"
du -sh ./content/records

echo "Building new builder"

cd builder && \
  cargo build --release

cd $ROOT

echo "Building image templates and processing images"

./builder/target/release/builder \
  --data-file ./data/DigitalHeritage.csv \
  --output-directory ./content/records \
  --images-directory ./static/records/images \
  --parallelism $(nproc)

echo "\n"

echo "Running zola to build the site"

$ZOLA --config ./config.toml build --base-url $BASE_URL

echo "Records directory size after build:"
du -sh ./content/records

echo "Search index size:"
du -sh ./public/search_index.en.js
