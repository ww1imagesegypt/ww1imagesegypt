+++
title = "Golygfeydd o Hen Dir"
description = "Delweddu’r Aifft a Phalestina yn y Rhyfel Byd Cyntaf"
+++

Mae Prosiect ‘Golygfeydd o’r Hen Dir’ wedi casglu delweddau o’r Aifft a Phalestina a gymerwyd yn ystod y Rhyfel Byd Cyntaf, ac mae'r rhain nawr ar gael i chi eu gweld a chynnig sylwadau arnynt.

Wrth gofio am y Rhyfel Byd Cyntaf, cofio am Ffrynt y Gorllewin y mae llawer, gan roi'r argraff bod y rhyfel yn un o fwd a ffosydd yn unig, gydag ychydig iawn o symudiad. Fodd bynnag, roedd y rhyfel yn yr Aifft a Phalestina yn llawer mwy symudol ac yn aml yn symud yn gyflym, ac roedd hefyd yn cael ei ymladd mewn amodau poeth a sych gan gyflwyno llawer o heriau i’r rheiny a oedd yn ymladd yno. Mae hefyd yn syndod i lawer bod cymaint o bersonél wedi gwasanaethu yn yr Aifft a Phalestina ar ryw adeg yn ystod y rhyfel gydag unedau yn cael eu tynnu'n ôl o Ffrynt y Gorllewin yn aml i wasanaethu yn yr ardal cyn dychwelyd i Ewrop yn nes ymlaen. Roedd yr Aifft hefyd yn gwasanaethu fel arhosfan ar gyfer yr Ymgyrch Dardanelles-Gallipoli, a Thessalonika. Y gobaith yw y bydd y delweddau yn archif parhaol i goffáu’r rheiny a wasanaethodd yn y theatr hon. Maent hefyd yn helpu i greu darlun o dirweddau’r Aifft a Phalestina ar y pryd ac yn gallu dangos sut mae'r trefi, y dinasoedd a’r safleoedd archeolegol wedi newid dros y blynyddoedd ers y rhyfel. Drwy hyn, byddant o ddiddordeb i'r rhai sy'n dymuno dysgu mwy am eu hynafiaid ac i haneswyr, archeolegwyr, athrawon ac eraill.

#### Ymwadiad

Mae gwefan ‘Golygfeydd o Hen Dir’ yn cyflwyno’r delweddau a'r dogfennau hyn fel rhan o gofnod o’r gorffennol. Mae'r delweddau a'r dogfennau hanesyddol hyn yn adlewyrchu agweddau a chredoau gwahanol gyfnod. Nid yw’r wefan yn cefnogi’r safbwyntiau a fynegir yn y casgliadau hyn, a all gynnwys deunyddiau sy'n peri tramgwydd i rai darllenwyr.
