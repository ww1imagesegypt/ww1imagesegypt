+++
title = "Y Wefan"
description = "Delweddau o’r Aifft a Phalestina – datgelu ffrynt y Dwyrain Canol, sy’n aml yn cael ei anghofio: Prosiect canmlwyddiant y Rhyfel Byd Cyntaf"
+++

Gyda chefnogaeth y Loteri drwy raglen Ein Treftadaeth Cronfa Dreftadaeth y Loteri, roedd ein prosiect yn canolbwyntio ar gasglu a chreu delweddau hygyrch o’r Aifft a Phalestina fel y byddant wedi cael eu gweld gan bobl yn ystod y Rhyfel Byd Cyntaf.

I nodi Canmlwyddiant y Rhyfel Byd Cyntaf, roedd cyfres o sioeau teithiol yng Nghymru a Lloegr ac fe ddatblygwyd gwefan ryngweithiol, gan alluogi'r tîm o wirfoddolwyr i gaffael a dehongli copïau o ffotograffau a gymerwyd yn yr Aifft a Phalestina gan aelodau'r lluoedd arfog neu a brynwyd ganddynt fel cardiau post ac y gellir eu dyddio i’r Rhyfel Byd Cyntaf. Cafodd y gwirfoddolwyr eu hyfforddi gan ddatblygu eu sgiliau yn y cyfryngau digidol a chyflwyno treftadaeth ac arwain at ddehongliad mwy cyflawn o’r Rhyfel Byd Cyntaf fel gwrthdaro gwirioneddol fyd-eang.

Roedd arddangosfeydd, gweithdai ysgolion a chynhadledd yn gyfleoedd i’r cyhoedd gymryd rhan uniongyrchol yn eu treftadaeth. Mae'r wefan yn adnodd dysgu ar-lein sy’n cynnig safbwyntiau newydd o safleoedd archeolegol, dinasoedd a gosodiadau milwrol wrth iddynt ymddangos yn ystod y rhyfel.

Meddai Paul Nicholson, Eifftolegydd ac Athro Archeoleg ym Mhrifysgol Caerdydd: "Mae hanes Prydain a'r Aifft wedi bod yn cydblethu ers tro byd. Fodd bynnag, yn aml caiff lle’r Aifft yn ystod y Rhyfel Byd Cyntaf ei anghofio oherwydd y pwyslais ar Ffrynt y Gorllewin. Roedd milwyr yn mynd drwy'r Aifft ar eu ffordd i Dardanelles, sef canolfan cudd-wybodaeth ar gyfer ymgyrchoedd yn erbyn yr Ymerodraeth Otomanaidd, ac yn benodol y safle lle lansiwyd Ymgyrch Palestina. Hyd yma, ychydig iawn o luniau sydd wedi bod ar gael yn ymwneud â'r gwrthdaro hwn, ac yn aml, roedd yn ymddangos na ellir cael unrhyw wybodaeth am beth yr oedd y bobl a oedd yn ymweld â'r rhanbarth yn ei feddwl ohoni a'r bobl oedd yn byw yno.”

Gyda help arbenigwyr, mae’r wybodaeth a gasglwyd wedi'i recordio'n ddigidol gan greu archif rhyngweithiol ar-lein lle gall pawb gael mynediad a chyfrannu gwybodaeth. Mae'r archif yn caniatáu i'r cyhoedd drafod, cyfrannu, rhannu ac ymchwilio i wybodaeth am yr Aifft a Phalestina yn y Rhyfel Byd Cyntaf.

Meddai Jennifer Stewart, Pennaeth Cronfa Dreftadaeth y Loteri yng Nghymru: "Ym mlwyddyn Canmlwyddiant y Rhyfel Byd Cyntaf, mae Cronfa Dreftadaeth y Loteri yn cefnogi llawer o brosiectau sy'n helpu pob un ohonom ni i archwilio a rhannu treftadaeth y Rhyfel Byd Cyntaf, er mwyn i ni gael gwell dealltwriaeth o’r effaith a gafodd o ran llunio’r Deyrnas Unedig a thu hwnt. Rwy’n falch iawn ein bod yn gallu cefnogi Prifysgol Caerdydd i ddarparu cyfleoedd i bobl ehangu eu dealltwriaeth o’r gwrthdaro a sut y mae wedi llunio ein byd modern"

Yn arwain y prosiect o Brifysgol Caerdydd mae Dr Steve Mills a’r Athro Paul Nicholson, y ddau o’r Ysgol Hanes, Archaeoleg a Chrefydd. Meddai Dr Mills: "Y tu hwnt i Ffrynt y Gorllewin, roedd Rhyfel Byd Cyntaf arall,  rhyfel o symud, o weithrediadau cafalri ac un a ymladdwyd yng ngwres yr anialwch ac yn nhrefi llychlyd y Dwyrain Canol. Rydym ni’n falch bod Cronfa Dreftadaeth y Loteri wedi dewis cefnogi'r prosiect hwn a fydd yn golygu bod mwy o bobl yn dod i wybod am y Rhyfel Byd Cyntaf arall hwn, yn arbennig disgynyddion y rhai a wasanaethodd yn yr Aifft a Phalestina."
