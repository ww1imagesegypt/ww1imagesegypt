+++
title = "Views of an Antique Land"
description = "Imaging Egypt and Palestine in the First World War"
+++

The Views of Antique Land Project has collected images of Egypt and Palestine taken during the First World War and these are now available for you to view.

Much of the commemoration of the First World War has focussed on the Western Front and so gives the impression that the war was entirely one of mud and trenches with very little movement. However, the war in Egypt and Palestine was much more mobile and often fast moving, it was also fought in hot and dry conditions and posed a whole range of challenges to those who fought there. It is also a surprise to many that such a great number of personnel did actually serve in Egypt and Palestine at some point during the war with units regularly being withdrawn from the Western Front to serve in the area before returning to Europe later on. Egypt also served as a staging post for the Dardanelles -Gallipoli- Campaign, and Thessalonika. It is hoped that the images will serve as an archive to permanently commemorate those who served in this theatre. They also help to build a picture of the landscapes of Egypt and Palestine at the time and can show how the towns, cities and archaeological sites have been altered over the years since the war. In this way they will be of interest to those who wish to learn more about their ancestors and to historians, archaeologists, teachers and others.

#### Disclaimer

The Views of an Antique Land website presents these images and documents as part of the record of the past. These primary historical images and documents reflect the attitudes and beliefs of different times. The Views of an Antique Land website does not endorse the views expressed in these collections, which may contain materials offensive to some readers.
