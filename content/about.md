+++
title = "About the project"
description = "Images of Egypt & Palestine – revealing the overlooked Middle-Eastern front: World War One centenary project"
+++

Supported by the Lottery through the Heritage Lottery Fund Our Heritage programme, our project focussed on collecting and making accessible images of Egypt and Palestine as they would have been seen by people during the First World War.

To mark the Centenary of the First World War, a series of roadshows in England and Wales along with the development of an interactive website enabled a team of volunteers to acquire and interpret copies of photographs taken in Egypt and Palestine by service personnel or bought by them as postcards and which can be dated to the First World War. The volunteers received training and developed skills in digital media and heritage presentation leading to a fuller interpretation of the First World War as a truly global conflict.

Exhibitions, school workshops and a conference provided opportunities for direct public participation in their heritage. The website is a perpetual online learning resource offering new views of archaeological sites, military installations and cities as they appeared during the war.

Egyptologist, Professor of Archaeology at Cardiff University, Paul Nicholson said: “The histories of Britain and Egypt have long been intertwined. However, the place of Egypt during the First World War is often overlooked because of the focus on the Western Front. Egypt was a transit point for troops en route for the Dardanelles, an intelligence base for campaigns against the Ottoman Empire, and in particular the base from which the Palestine Campaign was launched. Up until now access to images relating to this theatre of conflict have been limited and information on how the region was seen by those who visited it, and who lived there, has often seemed inaccessible.”

With help from specialists, the information gathered has been digitally recorded and an online interactive archive created where everyone can access and contribute information. The archive allows the public to discuss, contribute, share and research information about Egypt and Palestine in the First World War.

Jennifer Stewart, Head of the Heritage Lottery Fund in Wales said: “In this, First World War Centenary year, HLF is supporting many projects that are helping us all to explore and share the heritage of the First World War, so that we have a better understanding of the impact it had in shaping the UK and beyond. I am delighted that we are able to support Cardiff University to provide opportunities for people to broaden their understanding of the conflict and how it has shaped our modern world”

Leading the project from Cardiff University are Dr Steve Mills and Professor Paul Nicholson both of the School of History, Archaeology and Religion. Dr Mills said: “Beyond the Western Front there was another First World War, a war of movement, of cavalry operations and one which was fought in the heat of the deserts and in dusty towns in the Middle East. We are pleased that the Heritage Lottery Fund has chosen to support this project which will make this other First World War more widely known, especially to the descendants of those who served in Egypt and Palestine.”

### Find out more or get in touch

The email address for this project is [ww1imagesegypt@cardiff.ac.uk](mailto:ww1imagesegypt@cardiff.ac.uk).

You can also follow along with further developments in the project on our [Twitter](https://twitter.com/ww1imagesegypt) or [Facebook](https://www.facebook.com/ww1imagesegypt) accounts, or [check out our films on Vimeo](https://vimeo.com/channels/viewsofanantiqueland):

- [Twitter](https://twitter.com/ww1imagesegypt)
- [Facebook](https://www.facebook.com/ww1imagesegypt)
- [Vimeo](https://vimeo.com/channels/viewsofanantiqueland)

### Open Source

The structure of this website is open source under the GPLv3 license, and the photographs themselves published under the CC-BY-NC license.

The codebase for the website is [available on GitLab](https://gitlab.com/ww1imagesegypt/ww1imagesegypt) and contributions to improve it are welcomed.
