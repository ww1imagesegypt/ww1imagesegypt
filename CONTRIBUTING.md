Bugfixes and improvements to the features or functionality of the site are welcome and encouraged. Please create a fork and submit a merge request for contributions.

New images are not accepted via the GitLab repository as these need to go through the university team. Please contact the team at ww1imagesegypt@cardiff.ac.uk to discuss new content.