# Views of an Antique Land
## Imaging Egypt and Palestine in the First World War

### About the project

This is a project from [Cardiff University](https://www.cardiff.ac.uk).

Supported by the Lottery through the Heritage Lottery Fund Our Heritage programme, our project focussed on collecting and making accessible images of Egypt and Palestine as they would have been seen by people during the First World War.

Visit the site at [https://ww1imagesegypt.org.uk](https://ww1imagesegypt.org.uk).

### License

The code that builds the website is licensed under the GPLv3 license. The photographs themselves are licensed under CC-BY-NC, as agreed with their original contributors.

### About this repository

This repository contains all the images and the static-site builder used to build the website. The compiled site can be downloaded by clicking the `Download Artifacts` button on the latest `pages` job in [the CI jobs list](https://gitlab.com/ww1imagesegypt/ww1imagesegypt/-/jobs).

### How the site is built

There are two components to the build process:

1. A custom Rust application in the `builder` directory of this repository.
2. [Zola](https://www.getzola.org) is a static-site builder in Rust which generates the site based on content and template files.

### Requirements for building

Consult the [.gitlab-ci.yml](https://gitlab.com/ww1imagesegypt/ww1imagesegypt/-/blob/master/.gitlab-ci.yml) file for how the build process works.

The steps to running the builder are:

1. Clone this repository to your machine
2. Install Rust
3. Install Zola
4. Run `./build.sh zola`
