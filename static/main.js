document.addEventListener('DOMContentLoaded', function() {
  let galleryThumbs = document.querySelectorAll("[data-asset-target]");

  for (el of galleryThumbs) {
    el.addEventListener("click", function(event) {
      event.preventDefault();

      let target = this.dataset.assetTarget;
      let targetEl = document.querySelectorAll(`[data-asset = "${target}"]`)[0];
      let oldActive = targetEl.parentNode.querySelectorAll(".active");

      // Deactivate previous active element
      for (oldEl of oldActive) {
        oldEl.classList.remove("active");
      }

      // Make target active
      targetEl.classList.add("active");
    });
  }
}, false);
