use std::error::Error;
use std::fs;
use std::io::{self, Write};
use std::path::Path;
use std::sync::mpsc::channel;
use glob::glob;

use clap::Clap;
use serde::{Deserialize, Serialize};
use toml;
use image::imageops;
use threadpool::ThreadPool;

#[derive(Clap)]
#[clap(version = "1.0", author = "Robert May <rob@afternoonrobot.co.uk>")]
struct Opts {
    #[clap(short, long)]
    data_file: String,
    #[clap(short, long)]
    output_directory: String,
    #[clap(short, long)]
    images_directory: String,
    #[clap(short, long, default_value = "1")]
    parallelism: usize
}

#[derive(Clone, Debug, Deserialize, Serialize)]
struct Record {
    #[serde(alias = "guid")]
    guid: String,
    #[serde(alias = "nid", deserialize_with = "csv::invalid_option")]
    nid: Option<String>,
    #[serde(alias = "Title")]
    title: String,
    #[serde(alias = "Media Assets")]
    media_assets: Option<String>,
    #[serde(alias = "Sharing Settings", deserialize_with = "csv::invalid_option")]
    sharing_settings: Option<String>,
    #[serde(alias = "Sharing Protocols", deserialize_with = "csv::invalid_option")]
    sharing_protocols: Option<String>,
    #[serde(alias = "Category", deserialize_with = "csv::invalid_option")]
    category: Option<String>,
    #[serde(alias = "Summary", deserialize_with = "csv::invalid_option")]
    summary: Option<String>,
    #[serde(alias = "Creator", deserialize_with = "csv::invalid_option")]
    creator: Option<String>,
    #[serde(alias = "Contributor", deserialize_with = "csv::invalid_option")]
    contributor: Option<String>,
    #[serde(alias = "Date", deserialize_with = "csv::invalid_option")]
    date: Option<String>,
    #[serde(alias = "Cultural Narrative", deserialize_with = "csv::invalid_option")]
    cultural_narrative: Option<String>,
    #[serde(alias = "Traditional Knowledge", deserialize_with = "csv::invalid_option")]
    traditional_knowledge: Option<String>,
    #[serde(alias = "Description", deserialize_with = "csv::invalid_option")]
    description: Option<String>,
    #[serde(alias = "Keywords", deserialize_with = "csv::invalid_option")]
    keywords: Option<String>,
    #[serde(alias = "Publisher", deserialize_with = "csv::invalid_option")]
    publisher: Option<String>,
    #[serde(alias = "Rights", deserialize_with = "csv::invalid_option")]
    rights: Option<String>,
    #[serde(alias = "Traditional Licensing URL", deserialize_with = "csv::invalid_option")]
    traditional_licensing_url: Option<String>,
    #[serde(alias = "http://creativecommons.org/licenses/by-nc/4.0")]
    license_url: String,
    #[serde(alias = "Format", deserialize_with = "csv::invalid_option")]
    format: Option<String>,
    #[serde(alias = "Type")]
    r#type: Option<String>,
    #[serde(alias = "Identifier")]
    identifier: String,
    #[serde(alias = "Language")]
    language: String,
    #[serde(alias = "Source")]
    source: String,
    #[serde(alias = "Subject")]
    subject: String,
    #[serde(alias = "People")]
    people: Option<String>,
    #[serde(alias = "Transcription")]
    transcription: String,
    #[serde(alias = "Latitude", deserialize_with = "csv::invalid_option")]
    latitude: Option<f64>,
    #[serde(alias = "Longitude", deserialize_with = "csv::invalid_option")]
    longitude: Option<f64>,
    #[serde(alias = "Location Description")]
    location_description: String,
    #[serde(alias = "Collections")]
    collections: String,
    #[serde(alias = "Related Items", deserialize_with = "csv::invalid_option")]
    related_items: Option<String>,
    #[serde(alias = "Book Pages", deserialize_with = "csv::invalid_option")]
    book_pages: Option<String>,
    #[serde(alias = "Community Records", deserialize_with = "csv::invalid_option")]
    community_records: Option<String>
}

#[derive(Debug, Serialize)]
struct RefinedRecord {
    title: String,
    summary: String,
    description: String,
    taxonomies: Taxonomies,
    extra: Extra
}

#[derive(Debug, Serialize)]
struct Extra {
    guid: String,
    media_assets: Vec<String>,
    related_items: Vec<String>,
    license_url: String,
    license_short_code: String,
    location: Option<Location>,
    cultural_narrative: String,
    contributor: String
}

#[derive(Debug, Serialize)]
struct Location {
    latitude: f64,
    longitude: f64
}

#[derive(Debug, Serialize)]
struct Taxonomies {
    categories: Vec<String>,
    keywords: Vec<String>,
    people: Vec<String>
}

fn main() {
    let opts: Opts = Opts::parse();
    let pool = ThreadPool::new(opts.parallelism);
    let (tx, rx) = channel();

    let img_dir = opts.images_directory.clone();
    let records = load_csv(&opts.data_file).unwrap();
    let refined_records = records.iter().map(|record| refine_record(&record, &img_dir));

    for record in refined_records {
        let tx = tx.clone();
        let field_data = toml::Value::try_from(&record).unwrap();
        let content = format!("+++\n{}\n+++", field_data);
        let filename = format!("{}.md", &record.extra.guid);
        let path = Path::new(&opts.output_directory).join(filename);
        let img_dir = opts.images_directory.clone();

        pool.execute(move || {
            for image_name in record.extra.media_assets {
                let images_path = Path::new(&img_dir);
                let thumbnail_path = Path::new(&img_dir).join("thumbnails");
                let file = format!("{}.jpg", image_name);
                let path = images_path.join(file.clone());
                let img = image::open(path.clone());
                let thumb_file = thumbnail_path.join(file);

                if thumb_file.exists() {
                    continue;
                }

                match img {
                    Ok(img) => {
                        let thumbnail = img.resize(800, 800, imageops::FilterType::Lanczos3).crop(100, 100, 300, 300);

                        thumbnail.save(thumb_file).unwrap();
                        print!("i");
                    },
                    Err(_err) => print!("e"),
                }
            }

            match fs::write(path, &content) {
                Ok(()) => tx.send(".").unwrap(),
                Err(_e) => tx.send("e").unwrap(),
            }
        });
    }

    for _record in records {
        print!("{}", rx.recv().unwrap());
        io::stdout().flush().unwrap();
    }
}

fn load_csv(data_file: &str) -> Result<Vec<Record>, Box<dyn Error>> {
    let path = Path::new(data_file);
    let mut reader = csv::Reader::from_path(path).unwrap();
    let mut records: Vec<Record> = vec![];

    for line in reader.deserialize() {
        let record: Record = line?;

        records.push(record);
    }

    Ok(records)
}

fn find_media_assets_for(record: &Record, img_dir: &str) -> Vec<String> {
    let pattern = format!("{}/{}*", img_dir, record.guid.to_string());
    let mut assets = vec![];

    for entry in glob(&pattern).expect("Failed to read glob pattern") {
        match entry {
            Ok(path) => {
                match path.file_stem() {
                    Some(file_name) => assets.push(file_name.to_str().unwrap().to_string()),
                    None => println!("Encountered problem finding file")
                }
            },
            Err(e) => println!("{:?}", e),
        }
    }

    assets
}

fn refine_record(record: &Record, img_dir: &str) -> RefinedRecord {
    let mut media_assets = split_multi_value_string(&record.media_assets, ";");

    if media_assets.len() == 0 {
        println!("Finding missing assets for {}", record.guid.to_string());

        media_assets = find_media_assets_for(record, img_dir);
    }

    RefinedRecord {
        title: record.title.to_string(),
        summary: string_or_blank(&record.summary),
        description: string_or_blank(&record.description),
        taxonomies: Taxonomies {
            categories: split_multi_value_string(&record.category, ";"),
            keywords: split_multi_value_string(&record.keywords, ";"),
            people: split_multi_value_string(&record.people, ";")
        },
        extra: Extra {
            guid: record.guid.to_string(),
            media_assets: media_assets,
            related_items: split_multi_value_string(&record.related_items, ";"),
            license_url: record.license_url.to_string(),
            license_short_code: get_license_code(&record.license_url),
            location: build_location(&record),
            cultural_narrative: string_or_blank(&record.cultural_narrative),
            contributor: string_or_blank(&record.contributor)
        }
    }
}

fn get_license_code(_url: &String) -> String {
    // Hard-coded for now for simplicity, as everything has the same license
    "by-nc/4.0".to_string()
}

fn string_or_blank(string: &Option<String>) -> String {
    match string {
        Some(str) => str.to_string(),
        None => String::new(),
    }
}

fn split_multi_value_string(string: &Option<String>, delimiter: &str) -> Vec<String> {
    match string {
        Some(str) =>
            str.split(delimiter).map(|s| s.trim().to_string()).collect::<Vec<String>>(),
        None =>
            vec![],
    }
}

fn build_location(record: &Record) -> Option<Location> {
    if let (Some(lat), Some(lng)) = (record.latitude, record.longitude) {
        Some(Location { latitude: lat, longitude: lng })
    } else {
        None
    }
}
